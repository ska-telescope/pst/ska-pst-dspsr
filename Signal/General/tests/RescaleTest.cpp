/***************************************************************************
 *
 *   Copyright (C) 2024-2025 by Jesmigel Cantos, Andrew Jameson and Will Gauvin
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

#include "dsp/Detection.h"
#include "dsp/GtestMain.h"
#include "dsp/TimeSeries.h"

#include "dsp/RescaleTest.h"
#include "dsp/RescaleMeanStdCalculator.h"
#include "dsp/RescaleMedianMadCalculator.h"
#include "dsp/Memory.h"

#include "dsp/SignalStateTestHelper.h"

// from PSRCHIVE / EPSIC
#include "BoxMuller.h"
#include "ascii_header.h"

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_CUDA
#include "dsp/RescaleCUDA.h"
#include "dsp/TransferCUDATestHelper.h"
#endif

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>

//! main method passed to googletest
int main(int argc, char* argv[])
{
  return dsp::test::gtest_main(argc, argv);
}

namespace dsp::test {

RescaleTest::RescaleTest()
{
  input = new dsp::TimeSeries;
  output = new dsp::TimeSeries;

#ifdef HAVE_CUDA
  /* CUDA-specific resources are constructed even if unused because
     the constructor is called only once */
  device_input = new dsp::TimeSeries;
  device_output = new dsp::TimeSeries;
  device_memory = new CUDA::DeviceMemory;
  device_input->set_memory(device_memory);
  device_output->set_memory(device_memory);
#endif
}

void RescaleTest::SetUp()
{
  if (::testing::UnitTest::GetInstance()->current_test_info()->value_param() != nullptr)
  {
    auto param = GetParam();
    npol = param.npol;
    if (param.dump_scale_offset)
    {
      scale_offset_dump_filepath = "/tmp/RescaleTest_scale_offset_dump.dada";
      scale_offset_dump = new dsp::RescaleScaleOffsetDump(scale_offset_dump_filepath);

      scale_offset_file_helper = new dsp::test::RescaleScaleOffsetDumpTestHelper;
    }
  }
  double tsamp{0.000064};
  double day = 12345;
  double ss = 54;
  double fs = 0.222;
  MJD epoch(day, ss, fs);

  input->set_rate(1/tsamp);
  input->set_nchan(nchan);
  input->set_npol(npol);
  input->set_start_time(epoch);
  input->set_ndim(ndim);
  input->set_input_sample(0);
}

void RescaleTest::TearDown()
{
  if (!scale_offset_dump_filepath.empty())
    std::remove(scale_offset_dump_filepath.c_str());
}

RescaleTest::Mode RescaleTest::mode_name_to_mode(std::string mode_name)
{
  if (mode_name == "HalfWave")
    return RescaleTest::Mode::HalfWave;
  else if (mode_name == "LinearRamp")
    return RescaleTest::Mode::LinearRamp;
  else if (mode_name == "AllZero")
    return RescaleTest::Mode::AllZero;
  else
    return RescaleTest::Mode::None;
}

void RescaleTest::get_offset_scale(RescaleTest::Mode mode, uint64_t idat, unsigned ipol, unsigned ichan, float *offset, float *scale)
{
  *offset = 0.1 + (0.1 * ipol);
  *scale = 2.0 + (1 * ichan);

  if (mode == HalfWave)
  {
    if (idat < ndat/2)
      *scale += 1;
    else
      *scale -= 1;
  }
  else if (mode == LinearRamp)
  {
    // range from -1 to +1 over the full ndat
    float factor = ((2 * static_cast<float>(idat)) / static_cast<float>(ndat)) - 1;
    *scale += factor;
  }
  else if (mode == AllZero)
  {
    *offset = 0.0;
    *scale = 0.0;
  }
}

void RescaleTest::generate_fpt()
{
  input->set_order(dsp::TimeSeries::OrderFPT);
  output->set_order(dsp::TimeSeries::OrderFPT);
#ifdef HAVE_CUDA
  if (on_gpu)
  {
    device_input->set_order(dsp::TimeSeries::OrderFPT);
    device_output->set_order(dsp::TimeSeries::OrderFPT);
  }
#endif

  input->resize(ndat);

  std::vector<float> data(input->get_ndat() * input->get_ndim(), 0.0);
  time_t now = time(nullptr);
  BoxMuller bm(now);

  float offset{0}, scale{0};
  for (unsigned ipol=0; ipol<input->get_npol(); ipol++)
  {
    for (unsigned ichan=0; ichan<input->get_nchan(); ichan++)
    {
      std::generate(data.begin(), data.end(), bm);

      float * ptr = input->get_datptr(ichan, ipol);
      uint64_t ival = 0;
      for (uint64_t idat=0; idat<input->get_ndat(); idat++)
      {
        get_offset_scale(input_mode, idat, ipol, ichan, &offset, &scale);
        for (unsigned idim=0; idim<input->get_ndim(); idim++)
        {
          ptr[ival] = (data[idat] * scale) + offset;
          ival++;
        }
      }
    }
  }
}

void RescaleTest::generate_tfp()
{
  input->set_order(dsp::TimeSeries::OrderTFP);
  output->set_order(dsp::TimeSeries::OrderTFP);
#ifdef HAVE_CUDA
  if (on_gpu)
  {
    device_input->set_order(dsp::TimeSeries::OrderTFP);
    device_output->set_order(dsp::TimeSeries::OrderTFP);
  }
#endif

  input->resize(ndat);

  // generate the required number of normally distributed values with zero mean and unit variance
  size_t nval = ndat * input->get_ndim() * input->get_npol() * input->get_nchan();
  std::vector<float> data(nval, 0.0);
  time_t now = time(nullptr);
  BoxMuller bm(now);
  std::generate(data.begin(), data.end(), bm);

  float * ptr = input->get_dattfp();
  uint64_t ival = 0;
  float offset{0}, scale{0};
  for (unsigned idat=0; idat<ndat; idat++)
  {
    for (unsigned ichan=0; ichan<input->get_nchan(); ichan++)
    {
      for (unsigned ipol=0; ipol<input->get_npol(); ipol++)
      {
        get_offset_scale(input_mode, idat, ipol, ichan, &offset, &scale);
        for (unsigned idim=0; idim<input->get_ndim(); idim++)
        {
          ptr[ival] = (data[ival] * scale) + offset;
          ival++;
        }
      }
    }
  }
}

void RescaleTest::generate_data()
{
  if (order == dsp::TimeSeries::OrderFPT)
    EXPECT_NO_THROW(generate_fpt());
  else
    EXPECT_NO_THROW(generate_tfp());
}

void RescaleTest::assert_offsets_scales()
{
#ifdef HAVE_CUDA
  if (on_gpu)
  {
    output->zero();
    TransferCUDATestHelper xfer;
    xfer.copy(output, device_output, cudaMemcpyDeviceToHost);
  }
#endif

  float expected_offset{0}, expected_scale{0};
  uint64_t idat = 0;

  for (unsigned ipol=0; ipol<input->get_npol(); ipol++)
  {
    const float * offsets = rescale->get_offset(ipol);
    const float * scales = rescale->get_scale(ipol);
    for (unsigned ichan=0; ichan<input->get_nchan(); ichan++)
    {
      get_offset_scale(offsets_scales_mode, idat, ipol, ichan, &expected_offset, &expected_scale);
      expected_offset = -1 * expected_offset;
      expected_scale = expected_scale == 0 ? 1 : 1.0 / expected_scale;

      auto offset = offsets[ichan];
      auto scale = scales[ichan];

      EXPECT_NEAR(expected_offset, offsets[ichan], 0.1);
      EXPECT_NEAR(expected_scale, scales[ichan], 0.1);
    }
  }
}

void RescaleTest::assert_expected_rescaled_statistics(const std::vector<std::vector<double>> &sums, const std::vector<std::vector<double>> &sums_sq)
{
  // expected values for the mean, stddev and variance after the Rescale operation
  static constexpr double expected_rescaled_mean = 0.0;
  static constexpr double expected_rescaled_variance = 1.0;
  double nval = static_cast<double>(ndat) * output->get_ndim() ;

  double error_of_mean = sqrt(expected_rescaled_variance / nval);

  // for a normal distribution, where mu_4 = 3 * variance^2
  double error_of_variance = sqrt((3.0 - double(nval-3)/(nval-1)) / nval) * expected_rescaled_variance;

  static constexpr double threshold = 6.0; // sigma -> 3.4 false positives per million tests
  double assert_near_tolerance_mean = threshold * error_of_mean;
  double assert_near_tolerance_variance = threshold * error_of_variance;

  for (unsigned ichan=0; ichan<sums.size(); ichan++)
  {
    for (unsigned ipol=0; ipol<sums[ichan].size(); ipol++)
    {
      double mean = sums[ichan][ipol] / nval;
      double mean_sq = sums_sq[ichan][ipol] / nval;
      double variance = mean_sq - (mean * mean);

      ASSERT_NEAR(mean, expected_rescaled_mean, assert_near_tolerance_mean);

      // in cases where all data are zero, the rescale operation defines the variance as 1
      if (variance > 0)
      {
        ASSERT_NEAR(variance, expected_rescaled_variance, assert_near_tolerance_variance);
      }
    }
  }
}

void RescaleTest::assert_fpt()
{
  // compute the sum and sumsq for each channel/pol
  std::vector<std::vector<double>> sums(output->get_nchan());
  std::vector<std::vector<double>> sums_sq(output->get_nchan());
  for (unsigned ichan=0; ichan<output->get_nchan(); ichan++)
  {
    sums[ichan].resize(output->get_npol(), 0);
    sums_sq[ichan].resize(output->get_npol(), 0);
  }

  for (unsigned ipol=0; ipol<output->get_npol(); ipol++)
  {
    for (unsigned ichan=0; ichan<output->get_nchan(); ichan++)
    {
      float * ptr = output->get_datptr(ichan, ipol);
      uint64_t ival = 0;
      for (uint64_t idat=0; idat<output->get_ndat(); idat++)
      {
        for (unsigned idim=0; idim<output->get_ndim(); idim++)
        {
          const double val = static_cast<double>(ptr[ival]);
          sums[ichan][ipol] += val;
          sums_sq[ichan][ipol] += (val * val);
          ival++;
        }
      }
    }
  }
  assert_expected_rescaled_statistics(sums, sums_sq);
}

void RescaleTest::assert_tfp()
{
  float * ptr = output->get_dattfp();

  // compute the sum and sumsq for each channel/pol
  std::vector<std::vector<double>> sums(output->get_nchan());
  std::vector<std::vector<double>> sums_sq(output->get_nchan());
  for (unsigned ichan=0; ichan<output->get_nchan(); ichan++)
  {
    sums[ichan].resize(output->get_npol(), 0);
    sums_sq[ichan].resize(output->get_npol(), 0);
  }

  for (unsigned idat=0; idat<ndat; idat++)
  {
    for (unsigned ichan=0; ichan<output->get_nchan(); ichan++)
    {
      for (unsigned ipol=0; ipol<output->get_npol(); ipol++)
      {
        for (unsigned idim=0; idim<output->get_ndim(); idim++)
        {
          const double val = static_cast<double>(*ptr);
          sums[ichan][ipol] += val;
          sums_sq[ichan][ipol] += (val * val);
          ptr++;
        }
      }
    }
  }
  assert_expected_rescaled_statistics(sums, sums_sq);
}

void RescaleTest::assert_data()
{
#ifdef HAVE_CUDA
  if (on_gpu)
  {
    output->zero();
    TransferCUDATestHelper xfer;
    xfer.copy(output, device_output, cudaMemcpyDeviceToHost);
  }
#endif

  if (order == dsp::TimeSeries::OrderFPT)
    assert_fpt();
  else
    assert_tfp();

  if (dsp::Operation::verbose)
    std::cerr << "RescaleTest::assert_data all values are as expected" << std::endl;
}

void RescaleTest::assert_scale_offset_dump()
{
  scale_offset_file_helper->assert_file(scale_offset_dump_filepath);
}

bool RescaleTest::perform_transform()
  try
{
  if (!on_gpu)
  {
    rescale->set_input(input);
    rescale->set_output(output);
  }
#ifdef HAVE_CUDA
  else
  {
    TransferCUDATestHelper xfer;
    xfer.copy(device_input, input, cudaMemcpyHostToDevice);
    rescale->set_input(device_input);
    rescale->set_output(device_output);
  }
#endif

    rescale->prepare();
    rescale->operate();
    return true;
  }
catch (std::exception &exc)
{
    std::cerr << "Exception Caught: " << exc.what() << std::endl;
    return false;
  }
  catch (Error &error)
  {
    std::cerr << "Error Caught: " << error << std::endl;
    return false;
  }

void RescaleTest::new_transform_under_test()
{
  Reference::To<dsp::Rescale> rs = new dsp::Rescale;
#ifdef HAVE_CUDA
  if (on_gpu)
  {
    cudaStream_t stream;
    cudaStreamCreate(&stream);
    rs->set_engine(new CUDA::RescaleEngine(stream));
  }
#endif
  if (dump_scale_offset)
  {
    rs->scales_updated.connect(scale_offset_dump, &dsp::RescaleScaleOffsetDump::handle_scale_offset_updated);
    rs->scales_updated.connect(scale_offset_file_helper, &dsp::test::RescaleScaleOffsetDumpTestHelper::rescale_update);
  }

  rescale = std::shared_ptr<dsp::Rescale>(rs.release());
}

TEST_F(RescaleTest, test_construct_delete) // NOLINT
{
  new_transform_under_test();
  ASSERT_NE(rescale, nullptr);
  rescale = nullptr;
  ASSERT_EQ(rescale, nullptr);
}

TEST_P(RescaleTest, test_rescale_operation) // NOLINT
{
  auto param = GetParam();
  on_gpu = param.on_gpu;
  order = param.order;
  mode_name = param.mode;

  const auto calculator_name = param.calculator;
  dump_scale_offset = param.dump_scale_offset;

  offsets_scales_mode = input_mode = mode_name_to_mode(mode_name);
  if (input_mode == RescaleTest::Mode::LinearRamp || input_mode == RescaleTest::Mode::HalfWave)
    offsets_scales_mode = RescaleTest::Mode::None;

  state = param.state;
  npol = param.npol;
  ndim = get_ndim_for_state(state);

  input->set_state(state);
  input->set_npol(npol);
  input->set_ndim(ndim);
  new_transform_under_test();

  if (calculator_name == "MedianMad") {
    test_using_median_mad();
  }
  else
  {
    // ATM on_gpu only performs mean and std calculations.
    test_using_mean_std();
  }

  if (dump_scale_offset) {
    assert_scale_offset_dump();
  }
}

void RescaleTest::test_using_mean_std()
{
  if (dsp::Observation::verbose)
    std::cerr << "RescaleTest::test_using_mean_std mode=" << mode_name << " state=" << state_string(state) << " npol=" << npol << " ndim=" << ndim << std::endl;

  ScalesUpdatedMock callback_mock;
  if (!on_gpu)
  {
    const auto *calculator = rescale->get_calculator();
    rescale->scales_updated.connect(&callback_mock, &ScalesUpdatedMock::scales_updated);

    ASSERT_EQ(calculator->get_scales().size(), 0);
    ASSERT_EQ(calculator->get_offsets().size(), 0);
    EXPECT_CALL(callback_mock, scales_updated(testing::Eq(rescale.get()))).Times(1);
  }
  else
  {
    EXPECT_CALL(callback_mock, scales_updated(testing::_)).Times(0);
  }

  generate_data();
  ASSERT_TRUE(perform_transform());
  assert_offsets_scales();
  assert_data();

  if (!on_gpu)
    rescale->scales_updated.disconnect(&callback_mock, &ScalesUpdatedMock::scales_updated);
}

void RescaleTest::test_using_median_mad()
{
  // set up calculator
  auto *calculator = new dsp::RescaleMedianMadCalculator;
  rescale->set_calculator(calculator);

  // set up callback mock
  ScalesUpdatedMock callback_mock;
  rescale->scales_updated.connect(&callback_mock, &ScalesUpdatedMock::scales_updated);

  ASSERT_EQ(calculator->get_scales().size(), 0);
  ASSERT_EQ(calculator->get_offsets().size(), 0);

  generate_data();

  // GMOCK EXPECT_CALL must be setup before calls to the mock.
  EXPECT_CALL(callback_mock, scales_updated(testing::Eq(rescale.get())));

  ASSERT_TRUE(perform_transform());

  ASSERT_EQ(calculator->get_scales().size(), npol);
  ASSERT_EQ(calculator->get_offsets().size(), npol);

  // assert data
  for (auto ipol = 0; ipol < npol; ipol++)
  {
    // asserts we have the right number of nchan for the pol
    ASSERT_EQ(calculator->get_scales()[ipol].size(), nchan);
    ASSERT_EQ(calculator->get_offsets()[ipol].size(), nchan);

    auto scales = calculator->get_offset(ipol);
    auto offsets = calculator->get_offset(ipol);
    ASSERT_TRUE(scales != nullptr);
    ASSERT_TRUE(offsets != nullptr);

    for (auto ichan = 0; ichan < nchan; ichan++)
    {
      ASSERT_NE(scales[ichan], 0.0);
      ASSERT_NE(offsets[ichan], 0.0);
    }
  }

  // disconnect the callback_mock from the rescale callback
  rescale->scales_updated.disconnect(&callback_mock, &ScalesUpdatedMock::scales_updated);
}

std::vector<bool> get_gpu_flags()
{
#ifdef HAVE_CUDA
  int deviceCount;
  cudaError_t cudaStatus = cudaGetDeviceCount(&deviceCount);

  if (cudaStatus == cudaSuccess && deviceCount > 0)
  {
    return {false, true};
  }

  std::cout << "No GPU detected ... tests of CUDA::ChanPolSelectEngine disabled" << std::endl;
#endif
  return {false};
}

std::vector<Signal::State> get_states()
{
  return { Signal::Nyquist, Signal::Analytic, Signal::Intensity, Signal::PPQQ, Signal::Coherence };
}

std::vector<std::string> get_calculators_for_device(bool on_gpu)
{
  if (on_gpu)
    return {"GPU"};
  else
    return {"MeanStd", "MedianMad"};
}

std::vector<std::string> get_modes_for_calculator(std::string calculator)
{
  if (calculator == "MedianMad")
    return {"None"};
  else
    return {"None", "HalfWave", "LinearRamp", "AllZero"};
}

std::vector<dsp::test::TestParam> get_test_params()
{
  std::vector<dsp::test::TestParam> params{};

  for (auto on_gpu : get_gpu_flags())
  {
    std::vector<bool> dump_scale_offset_options{true, false};
    if (on_gpu)
      dump_scale_offset_options = {false};

    for (auto calculator : get_calculators_for_device(on_gpu))
    {
      if (on_gpu) {
        assert (calculator == "GPU");
      }

      for (auto mode : get_modes_for_calculator(calculator))
      {

        for (auto order : {dsp::TimeSeries::OrderFPT, dsp::TimeSeries::OrderTFP})
        {
          for (auto dump : dump_scale_offset_options)
          {
            for (auto state : get_states())
            {
              for (auto npol : get_npols_for_state(state))
              {
                params.push_back({ on_gpu, mode, order, calculator, state, npol, dump});
              }
            }
          }
        }
      }
    }
  }

  return params;
}

INSTANTIATE_TEST_SUITE_P(
    RescaleTestSuite, RescaleTest,
    testing::ValuesIn(get_test_params()),
    [](const testing::TestParamInfo<RescaleTest::ParamType> &info)
    {
      auto param = info.param;

      std::string name;
      if (param.on_gpu)
        name = "on_gpu";
      else
        name = "on_cpu";

      if (param.order == dsp::TimeSeries::OrderFPT)
        name += "_fpt_";
      else
        name += "_tfp_";

      if (!param.on_gpu)
      {
        if (param.calculator == "MeanStd")
          name += "mean_std_";
        else
          name += "median_mad_";
      }

      name += (param.mode + "_");

      name += State2string(param.state);
      name += "_npol";
      name += std::to_string(param.npol);

      if (param.dump_scale_offset)
        name += "_dump";
      else
        name += "_nodump";

      return name;
    }); // NOLINT

} // namespace dsp::test
