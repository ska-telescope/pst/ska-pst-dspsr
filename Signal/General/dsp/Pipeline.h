//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2011 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

// dspsr/Signal/General/dsp/Pipeline.h

#ifndef __dspsr_Pipeline_h
#define __dspsr_Pipeline_h

#include "Reference.h"
#include "environ.h"
#include "MJD.h"

namespace dsp {

  class Source;

  //! Abstract base class of data reduction pipelines

  class Pipeline : public Reference::Able
  {

  public:

    //! Set the Source from which data are obtained
    virtual void set_source (Source*) = 0;

    //! Get the Source from which data are obtained
    virtual Source* get_source () = 0;

    //! Build the data reduction pipeline
    virtual void construct () = 0;

    //! Prepare to process
    virtual void prepare () = 0;

    //! Seek such that output starts at the specified epoch
    virtual void seek_epoch (const MJD&) = 0;

    //! Run through the data
    virtual void run () = 0;

    //! Finish everything
    virtual void finish () = 0;

    //! Get the minimum number of samples required to process
    virtual uint64_t get_minimum_samples () const = 0;

  };
}

#endif // !defined(__Pipeline_h)
