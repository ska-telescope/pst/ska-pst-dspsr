//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2024 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

#ifndef __dspsr_LoadToSliceN_h
#define __dspsr_LoadToSliceN_h

#include "dsp/LoadToSlice.h"
#include "dsp/MultiThread.h"

class ThreadContext;

namespace dsp {

  class OutputFileShare;

  //! Multiple LoadToSlice threads
  class LoadToSliceN : public MultiThread, public CanSliceTime
  {

  public:

    //! Constructor
    LoadToSliceN (LoadToSlice::Config*);
    
    //! Set the number of thread to be used
    void set_nthread (unsigned);

    //! Set the configuration to be used in prepare and run
    void set_configuration (LoadToSlice::Config*);

    //! Setup sharing
    void share ();

    //! Set the start of the time slice
    void set_start_time (const MJD&) override;

    //! Set the end of the time slice
    void set_end_time (const MJD&) override;

  protected:

    //! Configuration parameters
    /*! call to set_configuration may precede set_nthread */
    Reference::To<LoadToSlice::Config> configuration;

    //! OutputFile sharing
    Reference::To<OutputFileShare> output_file;

    //! The creator of new LoadToSlice threads
    virtual LoadToSlice* new_thread ();

    //! Return the pointer to the specified LoadToSlice thread
    LoadToSlice* at (unsigned index);
  };

}

#endif // !defined(__LoadToSliceN_h)





