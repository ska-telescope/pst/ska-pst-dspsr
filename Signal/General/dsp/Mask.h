//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2023 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

#ifndef __dsp_Signal_General_Mask_h
#define __dsp_Signal_General_Mask_h

#include "dsp/Transformation.h"
#include "dsp/TimeSeries.h"

namespace dsp {

  class TimeSeries;

  //! Masks samples in the input TimeSeries by setting them to zero
  class Mask: public Transformation<TimeSeries,TimeSeries> {

  public:

    //! Default constructor
    Mask (const char* name);

    //! Destructor
    ~Mask ();

    //! Return true if the specified input data order can be supported
    bool get_order_supported (TimeSeries::Order order) const;

    //! Factory creates a new Mask-derived transformation object
    static Mask* factory (const std::string& descriptor);

  protected:

    //! Perform the transformation on the input time series
    void transformation ();

    //! Derived types define how masking is performed
    virtual void mask_data() = 0;

    //! Helper function masks all samples on the specified interval
    /*! All samples indexed by [start_idat, end_idat) are set to zero */
    virtual void mask (uint64_t start_idat, uint64_t end_idat);

    //! Mask data in Time-Frequency-Polarization order
    void mask_TFP(uint64_t start_idat, uint64_t end_idat);

    //! Mask data in Frequency-Polarization-Time order
    void mask_FPT(uint64_t start_idat, uint64_t end_idat);
  };

}

#endif // !defined(__dsp_Signal_General_Mask_h)
