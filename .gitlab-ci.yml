variables:
  DSPSR_BASE_IMAGE: registry.gitlab.com/ska-telescope/pst/ska-pst-dsp-tools/ska-pst-dspsr-builder:0.0.13
  DSPSR_CONTAINER_NAME: dspsr_unittest-$CI_COMMIT_SHORT_SHA
  DSPSR_IMAGE: $GITLAB_REGISTRY/dspsr:$CI_COMMIT_SHORT_SHA
  PSRHOME: /home/pst
  GITLAB_CI_TAG: k8srunner-psi-low
  GITLAB_REGISTRY: $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME

image: $SKA_K8S_TOOLS_BUILD_DEPLOY

stages:
  - build
  - test
  - pipeline-tests

.common_ci_config:
  artifacts:
    paths:
      - build
  tags:
    - $GITLAB_CI_TAG
  rules:
    - exists:
        - Dockerfile

build:
  extends:
    - .common_ci_config
  tags:
    - ska-default
  stage: build
  script:
    - echo "Authenticate to GitLab registry"
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - echo "Building Container Image DSPSR_IMAGE=$DSPSR_IMAGE"
    - echo "DSPSR_BASE_IMAGE=$DSPSR_BASE_IMAGE"
    - docker image build --build-arg DSPSR_BASE_IMAGE="$DSPSR_BASE_IMAGE" -t $DSPSR_IMAGE .
    - docker push $DSPSR_IMAGE

build-cpu:
  extends:
    - .common_ci_config
  stage: build
  image: $DSPSR_BASE_IMAGE
  script:
    - echo "CPU Build test"
    - ./bootstrap
    - |
      mkdir -p $PSRHOME/build/dspsr
      cd $PSRHOME/build/dspsr &&
      echo 'uwb dada sigproc dummy fits vdif ska1 cpsr2' > backends.list &&
      $CI_PROJECT_DIR/configure &&
      make -j$(nproc)

distcheck:
  extends:
    - .common_ci_config
  script:
    - echo "Execute make distcheck"
    - docker run --rm --name $DSPSR_CONTAINER_NAME-distcheck -u root $DSPSR_IMAGE bash -c 'cd $PSRHOME/build/dspsr; make distcheck'

scan:
  extends:
    - .common_ci_config
  stage: test
  script:
    - echo "Scan built $DSPSR_IMAGE"
    - docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v $CI_PROJECT_DIR/build:/root/.cache/ aquasec/trivy image $DSPSR_IMAGE
  allow_failure: true

include:
  - # DSPSR functional pipeline tests
    local: '.gitlab/ci/functional-pipeline-tests.gitlab-ci.yml'

  - # DSPSR unit tests
    local: '.gitlab/ci/unit-tests.gitlab-ci.yml'
