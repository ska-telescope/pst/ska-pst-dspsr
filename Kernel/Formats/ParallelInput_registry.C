/***************************************************************************
 *
 *   Copyright (C) 2023 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

/*! \file ParallelInput_registry.C
  \brief Register dsp::ParallelInput-derived classes for use in this file
    
  Classes that inherit dsp::ParallelInput may be registered for use by
  utilizing the Registry::List<dsp::ParallelInput>::Enter<Type> template class.
  Static instances of this template class should be given a unique
  name and enclosed within preprocessor directives that make the
  instantiation optional.  There are plenty of examples in the source code.

  \note Do not change the order in which registry entries are made
  without testing all of the file types.  Ensure that anything
  added performs a proper is_valid() test.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/*! SingleFile is built in */
#include "dsp/SingleFile.h"
static dsp::ParallelInput::Register::Enter<dsp::SingleFile> single_file;

/*! DualFile is built in */
#include "dsp/DualFile.h"
static dsp::ParallelInput::Register::Enter<dsp::DualFile> dual_file;

/*
  get_register is defined here to ensure that this file is linked
*/
dsp::ParallelInput::Register& dsp::ParallelInput::get_register ()
{
  return Register::get_registry();
}

