/***************************************************************************
 *
 *   Copyright (C) 2023 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

/*! \file ParallelUnpacker_registry.C
  \brief Register dsp::ParallelUnpacker-derived classes for use in this file
    
  Classes that inherit dsp::ParallelUnpacker may be registered for use by
  utilizing the Registry::List<dsp::ParallelUnpacker>::Enter<Type> template class.
  Static instances of this template class should be given a unique
  name and enclosed within preprocessor directives that make the
  instantiation optional.  There are plenty of examples in the source code.

  \note Do not change the order in which registry entries are made
  without testing all of the file types.  Ensure that anything
  added performs a proper matches() test.
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#if HAVE_ska1
#include "dsp/SKAParallelUnpacker.h"
static dsp::ParallelUnpacker::Register::Enter<dsp::SKAParallelUnpacker> ska;
#endif

/*! SingleUnpacker is built in */
#include "dsp/SingleUnpacker.h"
static dsp::ParallelUnpacker::Register::Enter<dsp::SingleUnpacker> single_file;

/*
  get_register is defined here to ensure that this file is linked
*/
dsp::ParallelUnpacker::Register& dsp::ParallelUnpacker::get_register ()
{
  return Register::get_registry();
}

