//-*-C++-*-
/***************************************************************************
 *
 *   Copyright (C) 2024 by Willem van Straten
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

// dspsr/Kernel/Classes/dsp/DADAHeader.h

#ifndef __dsp_DADAHeader_h
#define __dsp_DADAHeader_h

#include "dsp/OutputFile.h"

namespace dsp {

  /**
   * @brief Manages a DADA ASCII header block
   *
   */
  class DADAHeader
  {
  public:

    /**
     * @brief The default DADA ASCII header block size
     * 
     */
    static const unsigned default_header_size;

    /**
     * @brief Set the DADA header
     * 
     * @param ascii_header block of ASCII text that will be copied
     */
    void set_header(const char* ascii_header);

    /**
     * @brief Get the immutable DADA header
     * 
     */
    const char* get_header() const { return &(header.at(0)); }

    /**
     * @brief Get the mutable DADA header
     * 
     */
    char* get_header() { return &(header.at(0)); }

    /**
     * @brief resize the header
     *
     * @param size desired header size
     * 
     * If size is less than default_header_size, it will be set to default_header_size
     * If size is greater than default_header_size, it will be set to the smallest power of two that is larger than size
     */
    void resize(unsigned size = 0);

    /**
     * @brief Get the current header size
     * 
     */
    size_t size() { return header.size(); }

  protected:

    //! The ASCII header block
    mutable std::vector<char> header;

  };

} // namespace dsp

#endif // !defined(__dsp_DADAHeader_h)
