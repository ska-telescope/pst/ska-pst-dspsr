/***************************************************************************
 *
 *   Copyright (C) 2024 by Andrew Jameson
 *   Licensed under the Academic Free License version 2.1
 *
 ***************************************************************************/

#include "dsp/Operation.h"
#include "dsp/Observation.h"
#include "dsp/GtestMain.h"
#include "MJD.h"

#include <unistd.h>
#include <sstream>
#include <fstream>

namespace dsp::test {

auto test_data_dir() -> std::string&
{
  static std::string data_dir = ".";
  return data_dir;
}

auto test_data_file(std::string const& filename) -> std::string
{
  return test_data_dir() + "/" + filename;
}

auto gtest_main(int argc, char** argv) -> int
{
  // will process gtest options and pass on the rest
  ::testing::InitGoogleTest(&argc, argv);

  // turn on all verbose logs by default, helps test coverage/correctness on logging
  dsp::Observation::verbose = true;
  dsp::Operation::verbose = true;
  MJD::verbose = true;

  // process extra command line options;
  for (int i=0; i < argc; i++)
  {
    std::string const arg(argv[i]); // NOLINT
    if (arg == "--test_data")
    {
      if(++i < argc)
      {
        std::string const val(argv[i]); //NOLINT
        test_data_dir() = val;
      }
    }
    else if (arg == "--info")
    {
      dsp::Observation::verbose = false;
      dsp::Operation::verbose = false;
      MJD::verbose = false;
    }
    else if (arg == "--debug")
    {
      dsp::Operation::verbose = true;
    }
    else if (arg == "--trace")
    {
      dsp::Observation::verbose = true;
      dsp::Operation::verbose = true;
      MJD::verbose = true;
    }
    else if (arg == "--warn")
    {
      dsp::Observation::verbose = false;
      dsp::Operation::verbose = false;
      MJD::verbose = false;
    }
  }

  return RUN_ALL_TESTS();
}

} // namespace dsp::test
